package com.ruoyi.demo.feign.constant;

/**
 * @deprecated 由于使用人数较少 决定与 3.4.0 版本移除
 */
@Deprecated
public class FeignTestConstant {

	public static final String BAIDU_NAME = "baidu";

	public static final String BAIDU_URL = "http://www.baidu.com";

}
